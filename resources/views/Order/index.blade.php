<!-- @include('Order.view') -->
@extends('layouts.app')
<nav class="navbar navbar-expand-md navbar-light shadow-sm" style="background-color: #0079C2">
    <div class="container">
        <a class="navbar-brand text-white font-weight-bold" href="{{ url('/home') }}">
            Mediris - Referentes
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-white font-weight-bold" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <i class="fas fa-user"></i> {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}<span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Cerrar Sesión') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

    <div id="backdrop" style="position: fixed; width: 100%; height: 100%; background: rgba(0, 0, 0, 0.5); backdrop-filter: blur(0px); top: 0px; z-index: 999; display: none;">
        
    </div>
    <div class="container">
        <div class="card">
            <div class="card-body">
                <form id="form-search" action="#">
                    <div class="row">
                        <div class="form-group col">
                            <label for="institution">Intitución</label>
                            <select class="form-control" id="institution" name="institution">
                                @foreach($instituciones as $institucion)
                                    <option value = {{$institucion->id}}>{{$institucion->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col">
                            <label for="patient">Tipo de paciente</label>
                            <select class="form-control" id="patient" name="patient">
                                <option value = "0">--</option>
                                @foreach($tipoPacientes as $tipoPaciente)
                                    <option style="text-transform: capitalize;" value = {{$tipoPaciente->id}}>{{$tipoPaciente->description}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="start_date">Desde</label>
                            <input type="date" class="form-control" id="start_date" name="start_date">
                        </div>
                        <div class="form-group col">
                            <label for="end_date">Hasta</label>
                            <input type="date" class="form-control" id="end_date" name="end_date">
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary float-right btn-form" style="background-color: #0079C2"><i class="fas fa-search"></i> Buscar</button>
                </form>
            </div>
        </div>
    </div>

    <!-- SPINNER -->
    <div class="d-flex justify-content-center">
        <div id="spinner" class="spinner-border text-primary" style="width: 6rem; height: 6rem; display: none" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <!-- SPINNER -->

    <div class="div-tabla" style="visibility: hidden; margin-top: 50px; margin-left: 30px; margin-right: 30px;">
        <hr>
        <table id="table-data" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Fecha Realización</th>
                    <th>N° Orden</th>
                    <th>N° Solicitud</th>
                    <th>Cédula</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Sexo</th>
                    <th>Fecha Nacimiento</th>
                    <th>Procedimiento</th>
                    <th>Modalidad</th>
                    <th>Estatus</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody style="text-align: center;">
                <tr>
                    <!-- DATATABLE -->
                </tr>
            </tbody>
        </table>
    </div>


    <!-- MODAL -->
    <div class="modal fade bd-data-modal-lg" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="dataModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="form-group col">
                                <label for="cedula">Cédula</label>
                                <input type="text" class="form-control" id="cedula" name="cedula" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="fecha_nacimiento">Fecha nacimiento</label>
                                <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="edad">Edad</label>
                                <input type="text" class="form-control" id="edad" name="edad" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="sexo">Sexo</label>
                                <input type="text" class="form-control" id="sexo" name="sexo" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="peso">Peso</label>
                                <input type="text" class="form-control" id="peso" name="peso" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="altura">Altura</label>
                                <input type="text" class="form-control" id="altura" name="altura" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="telefonos">Telefonos</label>
                                <input type="text" class="form-control" id="telefonos" name="telefonos" readonly>
                            </div>
                        </div>
                        <hr>
                        <h5 class="modalidad text-center"></h5>
                        <div class="row">
                            <div class="form-group col">
                                <label for="fecha">Fecha realización</label>
                                <input type="text" class="form-control" id="fecha" name="fecha" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="orden">No. de orden</label>
                                <input type="text" class="form-control" id="orden" name="orden" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="solicitud">No. de solicitud</label>
                                <input type="text" class="form-control" id="solicitud" name="solicitud" readonly>
                            </div>
                            <div class="form-group col">
                                <label for="tipo_paciente">Tipo de paciente</label>
                                <input style="text-transform: capitalize;" type="text" class="form-control" id="tipo_paciente" name="tipo_paciente" readonly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="estatus">Estatus</label>
                                <input type="text" class="form-control" id="estatus" name="estatus" readonly>
                            </div>
                        </div>
                        <div class="arch_esc">
                            <hr>
                            <h5>Archivos Escaneados</h5>
                            <div id="forArchivos"></div>
                        </div>
                        <hr>
                        <div id="botones" style="float: right;">
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL -->

    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" defer></script>
    <script src="http://cdn.datatables.net/plug-ins/1.10.15/dataRender/datetime.js" defer></script>
    <script>
        $(document).ready(function(){
            
            setFechas();

            $('.dropdown-toggle').dropdown();
            $('[data-toggle="tooltip"]').tooltip();

            $("#start_date").change(function(){
                if($("#start_date").val() > $("#end_date").val()){
                    alert("Fecha de inicio no puede ser mayor a la fecha de fin")
                    document.getElementById("start_date").value = $("#end_date").val();
                }
            });


            $("#end_date").change(function(){
                if($("#end_date").val() < $("#start_date").val()){
                    alert("Fecha de fin no puede ser menor a la fecha de inicio")
                    document.getElementById("end_date").value = $("#start_date").val();
                }
            });


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.btn-form').click(function(e){

                e.preventDefault();

                $(".div-tabla").css("visibility", "visible");
                $("#backdrop").css("display", "block");
                $("#spinner").css("display", "block");

                var dataString = $('#form-search').serializeArray();
                var data = {};
                $(dataString).each(function(index, obj){
                    data[obj.name] = obj.value;
                });
        
                table = $('#table-data').DataTable({
                    destroy: true,
                    serverSide: false,
                    stateSave: true,
                    autoWidth: false,
                    aaSorting: [0, 'asc'], //Ordenar por columna 
                    ajax: {
                        method: 'POST',
                        url: 'ajaxRequest',
                        data: data,
                        async: true,
                        complete: function(){
                            $("#spinner").css("display", "none");
                            $("#backdrop").css("display", "none");
                        }
                    },
                    columns: [
                        {data: "issue_date"},
                        {data: "accession_number"},
                        {data: "id"},
                        {data: "patient_identification_id"},
                        {data: "patient_first_name"},
                        {data: "patient_last_name"},
                        {data: "sexo"},
                        {data: "birth_date"},
                        {data: "description"},
                        {data: "name"},
                        {
                            data: null,
                            bSortable: false,
                            mRender: function(data){

                                data.status = traducirStatus(data.status)
                                
                                var html = '<span>'+data.status+'</span>';

                                if(data.requested_procedure_status_id == '1'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 16%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '2'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 33%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '3'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '4'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 66%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '5'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 83%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else if(data.requested_procedure_status_id == '6'){
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }else{
                                    html += '<div class="progress">'
                                    html += '<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>'
                                    html += '</div>'
                                }

                                return html; 
                            }
                        },
                        {
                            data: null,
                            bSortable: false,
                            mRender: function(data){

                                var btn = '';

                                btn += '<button type="button" class="btn btn-primary" style="margin-top: 7px;" data-toggle="modal" data-target=".bd-data-modal-lg" data-accession_number="'+data.accession_number+'" data-nombres="'+data.patient_first_name+'" data-apellidos="'+data.patient_last_name+'" data-sexo="'+data.sexo+'" data-cedula="'+data.patient_identification_id+'" data-fecha_nacimiento="'+data.birth_date+'" data-numero_historia="'+data.additional_patient_history+'" data-peso="'+data.weight+'" data-altura="'+data.height+'" data-cellphone_number="'+data.cellphone_number+'" data-telephone_number="'+data.telephone_number+'" data-id_paciente="'+data.id+'" data-descripcion="'+data.description+'" data-modalidad="'+data.name+'" data-datos="'+data+'" data-fecharealizacion="'+data.issue_date+'" data-id_solicitud="'+data.id+'" data-estatus="'+data.status+'" data-id_estatus="'+data.requested_procedure_status_id+'" data-patient_type="'+data.patient_type+'" data-urlpacs="'+data.urlpacs+'" data-id_administrativo="'+data.id_administrativo+'" data-url_mediris="'+data.url_mediris+'"><i class="fas fa-search"></i></button>';

                                return btn; 
                            }
                        }
                    ],
                    columnDefs: [
                        {
                            targets: 0, render:function(data){
                                return formatFecha(data, true, true);
                            }
                        },
                        {
                            targets: 6, render:function(data){ 
                                return data == 'F' ? 'Femenino' : 'Masculino';
                            }
                        },
                        {
                            targets: 7, render:function(data){
                                return formatFecha(data, true);
                            }
                        },
                    ],
                    language: {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
            });


            $('#dataModal').on('show.bs.modal', function (event) {
                
                var button = $(event.relatedTarget)
                
                var nombres = button.data('nombres')
                var apellidos = button.data('apellidos')
                var sexo = button.data('sexo')
                var cedula = button.data('cedula')
                var fecha_nacimiento = button.data('fecha_nacimiento')
                var numero_historia = button.data('numero_historia')
                var peso = button.data('peso')
                var altura = button.data('altura')
                var cellphone_number = button.data('cellphone_number')
                var telephone_number = button.data('telephone_number')
                var accession_number = button.data('accession_number')
                var id_paciente = button.data('id_paciente')
                var modalidad = button.data('modalidad')
                var descripcion = button.data('descripcion')
                var fecharealizacion = button.data('fecharealizacion')
                var id_solicitud = button.data('id_solicitud')
                var estatus = button.data('estatus')
                var id_estatus = button.data('id_estatus')
                var patient_type = button.data('patient_type')
                var urlpacs = button.data('urlpacs')
                var id_administrativo = button.data('id_administrativo')
                var url_mediris = button.data('url_mediris')
                
                $.ajax({
                    context: this,
                    async: true,
                    type: "POST",
                    data: {id_solicitud: id_solicitud, accession_number: accession_number},
                    url: "searchOrden",
                    success: function(datos) {
                        
                        /* Asignar valores a la modal */
                        var modal = $(this)
                        modal.find('.modal-title').text(apellidos + ", " + nombres)
                        modal.find('.modal-body #historia').val(numero_historia)
                        modal.find('.modal-body #cedula').val(cedula)

                        //modal.find('.modal-body #fecha_nacimiento').val(moment(fecha_nacimiento).format('DD-MM-YYYY'))
                        modal.find('.modal-body #fecha_nacimiento').val(formatFecha(fecha_nacimiento, true))

                        //modal.find('.modal-body #edad').val(moment().diff(moment(fecha_nacimiento), 'years'))
                        modal.find('.modal-body #edad').val(calcularEdad(fecha_nacimiento))

                        modal.find('.modal-body #sexo').val(sexo == 'F' ? 'Femenino' : 'Masculino')
                        modal.find('.modal-body #peso').val(peso == 0 ? "S/E" : parseFloat(peso).toFixed(2))
                        modal.find('.modal-body #altura').val(altura == 0 ? "S/E" : parseFloat(altura).toFixed(2))
                        modal.find('.modal-body #telefonos').val(formatearDatos(cellphone_number, telephone_number, 'telefono'))
                        
                        //modal.find('.modal-body #fecha').val(moment(fecharealizacion).format('DD-MM-YYYY hh:mm A'))
                        modal.find('.modal-body #fecha').val(formatFecha(fecharealizacion, true, true))

                        modal.find('.modal-body #orden').val(accession_number)
                        modal.find('.modal-body #solicitud').val(id_solicitud)
                        modal.find('.modal-body #estatus').val(estatus)
                        modal.find('.modal-body .modalidad').text(descripcion + "(" + modalidad + ")")
                        modal.find('.modal-body #tipo_paciente').val(patient_type)

                        if(id_estatus == '2'){
                            modal.find('.modal-body .btn-informe').hide()
                            modal.find('.modal-body .btn-img').hide()
                        }else if(id_estatus == '3'){
                            modal.find('.modal-body .btn-informe').hide()
                        }

                        modal.find('.modal-body #forArchivos').empty()

                        if(datos['files'].length == 0){
                            modal.find('.modal-body .arch_esc').hide()
                        }else{
                            modal.find('.modal-body .arch_esc').show()

                            for(let x in datos['files']){
                                // Boton para descargar los archivos adjuntos asociados a la orden
                                modal.find('.modal-body #forArchivos').append('<a href="'+url_mediris+'/storage/documentos/documents/'+datos['files'][x]['name']+'" target="_blank"><img src="'+url_mediris+'/storage/documentos/documents/'+datos['files'][x]['name']+'" height="42" width="42" style="margin-right:5px"></a>')
                            }
                        }

                        modal.find('.modal-body #botones').empty()

                        if(id_estatus == '6'){

                            // Boton para descargar el informe de la orden
                            if (datos['informe']){
                                url = '<a href="{{ url('searchReport/#accession')}}" target="_blank" class="btn btn-secondary btn-lg active btn-informe" role="button" aria-pressed="true" data-toggle="tooltip" data-placement="top" title="Descargar Informe" style="margin-right: 5px;"><i class="fas fa-clipboard-list"></i></a>';
                                url = url.replace("#accession", accession_number + '/O/' + id_administrativo);
                                modal.find('.modal-body #botones').append(url)
                            }

                            // Boton para descargar el addendum de la orden
                            if(datos['addendum']){
                                urlA = '<a href="{{ url('searchReport/#accession')}}" target="_blank" class="btn btn-secondary btn-lg active btn-informe" role="button" aria-pressed="true" data-toggle="tooltip" data-placement="top" title="Descargar Addendum" style="margin-right: 5px;"><i class="fas fa-clipboard-list"></i></a>';
                                urlA = urlA.replace("#accession", accession_number + '/A/' + id_administrativo);
                                modal.find('.modal-body #botones').append(urlA)
                            }
                        }

                        /*if(id_estatus == '3' || id_estatus == '4' || id_estatus == '5' || id_estatus == '6'){

                            // Boton para ver los estudios asociados a la orden
                            modal.find('.modal-body #botones').append('<a href="'+urlpacs+'" target="_blank" class="btn btn-secondary btn-lg active btn-img open" role="button" aria-pressed="true" data-toggle="tooltip" data-placement="top" title="Ver estudio" target="_parent"><i class="fas fa-film"></i></a>')
                        }*/
                    },
                    error: function() {}
                });
            })

            function setFechas(){
                today = formatFecha();
                document.getElementById("start_date").value = today;
                document.getElementById("end_date").value = today;
                $("#start_date, #end_date").attr({
                    "max" : today
                });
            }

            function formatearDatos(dato1, dato2, texto){

                var info = "";

                if(texto == 'telefono'){
                    if(dato1){
                        info = dato1;
                        if(dato2){
                            info += " / " + dato2;
                        }
                    }else{
                        if(dato2){
                            info = dato2;
                        }else{
                            info = "S/E";
                        }
                    }
                }else{
                    if(dato1 == 0){
                        info = "S/E";
                    }else{
                        info = parseFloat(dato1).toFixed(2);
                    }
                }

                return info;
            }


            function traducirStatus(status){
                if(status == 'to-admit'){
                    status = 'Por admitir'
                }else if(status == 'to-make'){
                    status = 'Por realizar'
                }else if(status == 'to-do'){
                    status = 'Por realizar'
                }else if(status == 'to-dictate'){
                    status = 'Por dictar'
                }else if(status == 'to-transcribe'){
                    status = 'Por transcribir'
                }else if(status == 'to-approve'){
                    status = 'Por aprobar'
                }else if(status == 'finished'){
                    status = 'Terminada'
                }else if(status == 'suspended'){
                    status = 'Suspendida'
                }

                return status
            }

            function formatFecha(fecha = '', ordenado = '', hora = ''){
                if(fecha){
                    var date = new Date(fecha)
                }else{
                    var date = new Date()
                }

                var day = date.getDate()
                var month = date.getMonth() + 1
                var year = date.getFullYear()

                month = month < 10 ? '0' + month : month
                day = day < 10 ? '0' + day : day

                if(ordenado){
                    if(hora){
                        hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
                        minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
                        ampm = hours >= 12 ? 'PM' : 'AM';

                        return day+'-'+month+'-'+year+' '+hours+':'+minutes+' '+ampm
                    }else{
                        return day+'-'+month+'-'+year
                    }
                }else{
                    return year+'-'+month+'-'+day
                }
            }

            function calcularEdad(fecha) {
                var hoy = new Date();
                var cumpleanos = new Date(fecha);
                var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                var m = hoy.getMonth() - cumpleanos.getMonth();

                if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                    edad--;
                }

                return edad;
            }
        });
    </script>

@endsection
