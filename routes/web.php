<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group([ 'prefix' => '/', 'middleware' => 'auth' ], function () {

    /**
 	* Routes Orders
 	*/ 
    
    Route::get('order', [ 'as' => 'order', 'uses' => 'OrderController@index' ]);

    Route::post('ajaxRequest', [ 'as' => 'ajaxRequest', 'uses' => 'OrderController@ajaxRequestPost' ]);

    Route::post('searchOrden', [ 'as' => 'searchOrden', 'uses' => 'OrderController@searchOrderPost' ]);

    Route::get('searchReport/{id}/{t}/{institucion}', [  'as' => 'searchReport/{id}/{t}/{institucion}', 'uses' => 'OrderController@searchReportPost' ]);
    
});
