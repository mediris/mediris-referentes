<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Old_records_ordenes extends Model{
    protected $fillable = ['fecharealizacion', 'accession_number', 'id_solicitud','id_usr','id_procedimiento','id_estatus'];
}
