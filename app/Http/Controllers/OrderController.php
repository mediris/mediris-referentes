<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Old_records_ordenes;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;

class OrderController extends Controller{
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $sqlInstitution = "SELECT * FROM `mediris`.`charges` where user_id = '".auth()->user()->id."'";
        $datos = DB::select($sqlInstitution);

        $instituciones = [];
        foreach ($datos as $key => $value) {
            $sql = "SELECT id, name FROM institutions where id = '".$value->institution_id."'";
            $institucion = DB::select($sql);
            array_push($instituciones, $institucion[0]);
        }

        $sql1 = "SELECT id, description FROM `apimeditron`.`patient_types`";

        $tipoPacientes = DB::select($sql1);

        return view('Order.index', ['instituciones' => $instituciones, 'tipoPacientes' => $tipoPacientes]);
    }


    public function ajaxRequestPost(Request $request){

        $datos = $request->all();

        $datos['start_date'] .= ' 00:00:00';
        $datos['end_date'] .= ' 23:59:59';

        $sql = "SELECT T1.issue_date, T1.id, T1.patient_id, T1.patient_identification_id, T1.patient_first_name, T1.patient_last_name, T1.patient_sex_id, T1.weight, T1.height, T1.patient_type_id, T2.id as accession_number, T2.procedure_id, T2.requested_procedure_status_id, T3.administrative_ID as sexo, T4.birth_date, T4.additional_patient_history, T4.cellphone_number, T4.telephone_number, T5.description, T5.modality_id, T6.name, T7.description as status, T8.description as patient_type FROM `apimeditron`.`service_requests` T1 LEFT JOIN `apimeditron`.`requested_procedures` T2 ON(T2.service_request_id = T1.id) LEFT JOIN `mediris`.`sexes` T3 ON(T3.id = T1.patient_sex_id) LEFT JOIN `mediris`.`patients` T4 ON(T4.id = T1.patient_id) LEFT JOIN `apimeditron`.`procedures` T5 ON(T5.id = T2.procedure_id) LEFT JOIN `apimeditron`.`modalities` T6 ON(T6.id = T5.modality_id) LEFT JOIN `apimeditron`.`requested_procedure_statuses` T7 ON(T7.id = T2.requested_procedure_status_id) LEFT JOIN `apimeditron`.`patient_types` T8 ON(T8.id = T1.patient_type_id) WHERE T1.issue_date BETWEEN '".$datos['start_date']."' AND '".$datos['end_date']."' and T1.institution_id = '".$datos['institution']."'";

        if($datos['patient'] != '0'){
            $sql.= " AND T1.patient_type_id = '".$datos['patient']."'";
        }

        if($this->getRoleReferring($datos['institution']) != '1' && $this->getRoleReferring($datos['institution']) != '99'){
            if(count($this->getIdReferring()) > 0){
                $sql.= " AND T1.referring_id = '".$this->getIdReferring()[0]->id."'";
            }
        }

        /*if(count($this->getIdReferring()) > 0){
            $data = DB::select($sql);
        }else{
            $data = [];
        }*/

        $data = DB::select($sql);

        $id_administrativo = $this->obtenerIdAdministrativo($datos['institution']);
     
        foreach ($data as $key => $value){

            /*$patientIdPacs = $this->getPatientId($value->accession_number, $id_administrativo);
            $value->cedulapacs = $patientIdPacs;
            $value->urlpacs = $this->buildUrl($patientIdPacs, $value->accession_number, $id_administrativo);*/
            $value->id_administrativo = $id_administrativo;
            $value->url_mediris = config('exa.sedes.'.$id_administrativo.'.url');
        }

        $result = array('aaData'=>$data);
        $data = json_encode($result);

        return $data;
    }

    public function searchOrderPost(Request $request){

        $datos = $request->all();

        $sql = "SELECT type, name, description, location FROM `apimeditron`.`request_documents` WHERE service_request_id = '".$datos['id_solicitud']."'";

        $info = DB::select($sql);

        if(count($info) > 0){
            $response['files'] = $info;
        }else{
            $response['files'] = [];
        }

        $response['addendum'] =  $this->validarAddendums($datos['accession_number']);
        $response['informe'] =  $this->validarInforme($datos['accession_number']);

        return $response;
    }


    public function searchReportPost($id, $t, $institucion){

        date_default_timezone_set('America/Caracas');
        setlocale(LC_TIME, 'es_ES.UTF-8');
        setlocale(LC_TIME, 'spanish');

        $firma = "";
        $url = config('exa.sedes.'.$institucion.'.url').'/storage/';

        if($t == 'O'){

            // $sql = "SELECT T1.id as orden, T1.service_request_id, T1.procedure_id, T1.text, T1.radiologist_user_name, T1.radiologist_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T3.administrative_ID as sexo, T4.birth_date, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T10.report_header, T10.report_footer FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN sexes T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN patients T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN users T7 ON(T7.id = T1.radiologist_user_id) LEFT JOIN prefixes T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN institutions T10 ON(T10.id = '".config('exa.sedes.'.$institucion.'.id')."') WHERE T1.id = '".$id."'";

            $sql = "SELECT T1.id as orden, T1.service_request_id, T1.procedure_id, T1.text, T1.approve_user_name, T1.approve_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T3.administrative_ID as sexo, T4.birth_date, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T7.mpps, T7.cdmc, T7.administrative_ID as ci, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T10.report_header, T10.report_footer FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN sexes T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN patients T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN users T7 ON(T7.id = T1.approve_user_id) LEFT JOIN prefixes T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN institutions T10 ON(T10.id = '".config('exa.sedes.'.$institucion.'.id')."') WHERE T1.id = '".$id."'";
        }else{

            // $sql = "SELECT T1.id as orden, T1.service_request_id, T1.procedure_id, T1.radiologist_user_name, T1.radiologist_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T3.administrative_ID as sexo, T4.birth_date, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T10.report_header, T10.report_footer, T11.text FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN sexes T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN patients T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN users T7 ON(T7.id = T1.radiologist_user_id) LEFT JOIN prefixes T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN institutions T10 ON(T10.id = '".config('exa.sedes.'.$institucion.'.id')."') LEFT JOIN `apimeditron`.`addendums` T11 ON(T11.requested_procedure_id = T1.id) WHERE T1.id = '".$id."'";

            $sql = "SELECT T1.id as orden, T1.service_request_id, T1.procedure_id, T1.approve_user_name, T1.approve_user_id, T1.culmination_date, T2.id, T2.patient_first_name, T2.patient_last_name, T2.patient_sex_id, T2.patient_identification_id, T2.patient_id, T2.patient_type_id, T2.referring_id, T2.issue_date, T3.administrative_ID as sexo, T4.birth_date, T5.description as patient_type, T6.description as procedimiento, T7.prefix_id, T7.position, T7.signature, T7.mpps, T7.cdmc, T7.administrative_ID as ci, T8.name as prexif, T9.first_name as nombre_referido, T9.last_name as apellido_referido, T10.report_header, T10.report_footer, T11.text FROM `apimeditron`.`requested_procedures` T1 LEFT JOIN `apimeditron`.`service_requests` T2 ON(T2.id = T1.service_request_id) LEFT JOIN sexes T3 ON(T3.id = T2.patient_sex_id) LEFT JOIN patients T4 ON(T4.id = T2.patient_id) LEFT JOIN `apimeditron`.`patient_types` T5 ON(T5.id = T2.patient_type_id) LEFT JOIN `apimeditron`.`procedures` T6 ON(T6.id = T1.procedure_id) LEFT JOIN users T7 ON(T7.id = T1.approve_user_id) LEFT JOIN prefixes T8 ON(T8.id = T7.prefix_id) LEFT JOIN `apimeditron`.`referrings` T9 ON(T9.id = T2.referring_id) LEFT JOIN institutions T10 ON(T10.id = '".config('exa.sedes.'.$institucion.'.id')."') LEFT JOIN `apimeditron`.`addendums` T11 ON(T11.requested_procedure_id = T1.id) WHERE T1.id = '".$id."'";
        }

        $info = DB::select($sql);

        $texto = html_entity_decode($info[0]->text);

        //$texto = preg_replace("/background-color[\s]*:[\s]*#[\w]+;?/i", "", $texto); //background-color:#ffffff;
        $texto = preg_replace("/box-sizing[\s]*:[\s]*[\w-]+[;]?/i", "", $texto);     //box-sizing : border-box;
        $texto = preg_replace("/text-autospace[\s]*:[\s]*[\w]+[;]?/i", "", $texto);  //text-autospace : none;
        $texto = preg_replace("/<img([\w\W]+?)\/>/i", "", $texto);

        $texto = preg_replace("/<FONT[^>]+>/i", "", $texto);
        $texto = preg_replace("/<\/FONT>/i", "", $texto);
        $texto = preg_replace("/<w:[^>]*>/", "<span>", $texto);
        $texto = preg_replace("/<\/w:[^>]*>/", "</span>", $texto);
        $texto = preg_replace("/<BR>/i", "", $texto);
        $texto = preg_replace("/<\/B>/i", "", $texto);
        $texto = preg_replace("/<B[\s\S]>/i", "", $texto);
        $texto = preg_replace("/tab-stops:[^>]*;/", "", $texto);
        $texto = preg_replace("/border-image:[^>]*;/", "", $texto);

        $texto = preg_replace("/style=\"text-decoration-line:underline;\"/i", "style=\"text-decoration:underline\"", $texto);
        
        $texto = preg_replace("/o:p/i", "p", $texto);

        if($info[0]->signature){
            $fileArray = pathinfo($info[0]->signature); 
            $nameImg = $fileArray['filename'];
            $extImg = $fileArray['extension'];

            $firma = $url. "firmas/signatures/".$nameImg.".".$extImg;
        }

        // $data = ['texto' => $texto, 'nombres' => $info[0]->patient_first_name, 'apellidos' => $info[0]->patient_last_name, 'cedula' => $info[0]->patient_identification_id, 'sexo' => $this->obtenerSexo($info[0]->sexo), 'fecha_nacimiento' => date('d-m-Y', strtotime($info[0]->birth_date)), 'accession_number' => $info[0]->orden, 'descripcion' => $info[0]->procedimiento, 'fecha_creacion' =>  strftime("%B %d, %Y", strtotime(date($info[0]->issue_date))), 'firma' => $firma, 'doctor' => $info[0]->prexif ." ". $info[0]->radiologist_user_name, 'patient_type' => $info[0]->patient_type, 'referido' => $this->obtenerReferido($info[0]->nombre_referido, $info[0]->apellido_referido), 'especialidad' => $info[0]->position, 'edad' => $this->calculaEdad(date('Y-m-d', strtotime($info[0]->birth_date))), 'fecha_dictado' => strftime("%B %d, %Y", strtotime(date($info[0]->culmination_date))), 'header_img' => $url . "" . $info[0]->report_header, 'footer_img' => $url . "" . $info[0]->report_footer ];

        $data = ['texto' => $texto, 'nombres' => $info[0]->patient_first_name, 'apellidos' => $info[0]->patient_last_name, 'cedula' => $info[0]->patient_identification_id, 'sexo' => $this->obtenerSexo($info[0]->sexo), 'fecha_nacimiento' => date('d-m-Y', strtotime($info[0]->birth_date)), 'accession_number' => $info[0]->orden, 'descripcion' => $info[0]->procedimiento, 'fecha_creacion' =>  strftime("%B %d, %Y", strtotime(date($info[0]->issue_date))), 'firma' => $firma, 'doctor' => $info[0]->prexif ." ". $info[0]->approve_user_name, 'patient_type' => $info[0]->patient_type, 'referido' => $this->obtenerReferido($info[0]->nombre_referido, $info[0]->apellido_referido), 'especialidad' => $info[0]->position, 'edad' => $this->calculaEdad(date('Y-m-d', strtotime($info[0]->birth_date))), 'fecha_dictado' => strftime("%B %d, %Y", strtotime(date($info[0]->culmination_date))), 'header_img' => $url . "" . $info[0]->report_header, 'footer_img' => $url . "" . $info[0]->report_footer, 'mpps' => $info[0]->mpps, 'cdmc' => $info[0]->cdmc, 'ci' => $info[0]->ci];

        $pdf = PDF::loadView('reporte', $data);

        if($t == 'O'){
            $nombre = "Informe Orden N° ".$info[0]->orden.'.pdf';
        }else{
            $nombre = "Addendum Orden N° ".$info[0]->orden.'.pdf';
        }

        $pdf->getDomPDF()->setHttpContext(
            stream_context_create([
                'ssl' => [
                    'allow_self_signed'=> TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                ]
            ])
        );

        return $pdf->download($nombre);
    }


    public function calculaEdad($fechaNacimiento){
        list($ano, $mes, $dia) = explode("-", $fechaNacimiento);
        
        $anoDiferencia  = date("Y") - $ano;
        $mesDiferencia = date("m") - $mes;
        $diaDiferencia   = date("d") - $dia;
        
        if ($diaDiferencia < 0 || $mesDiferencia < 0){
            $anoDiferencia--;
        }
        
        return $anoDiferencia;
    }


    public function obtenerSexo($sexo){

        if($sexo == 'F'){
            $descripcion = "Femenino";
        }else{
            $descripcion = "Masculino";
        }

        return $descripcion;
    }


    public function obtenerReferido($nombre, $apellido){

        if($nombre && $apellido){
            $referido = $nombre ." ". $apellido;
        }else{
            $referido = "No Especificado";
        }

        return $referido;
    }


    public function validarAddendums($orden){

        $sql = "SELECT * FROM `apimeditron`.`addendums` WHERE requested_procedure_id = '".$orden."'";
        $addendum = DB::select($sql);

        return count($addendum) > 0 ? true : false;
    }


    public function validarInforme($orden){

        $sql = "SELECT * FROM `apimeditron`.`requested_procedures` WHERE id = '".$orden."'";
        $informe = DB::select($sql);

        return $informe[0]->text != '' ? true : false; 
    }


    public function getPatientId($id, $institucion){
        $pacs = config('exa.sedes.'.$institucion.'.pacs');

        $posDomain = strpos($pacs, 'exa');
        $domainTemp = substr($pacs, $posDomain);
        $ip = gethostbyname($domainTemp);

        $url = "http://".$ip.":9669/pid_query/?accession_no=".$id;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $patient_id = curl_exec($ch);

        curl_close($ch);

        return $patient_id == '0' ? '0' : $patient_id;
    }


    public function buildUrl($patientid, $ordernum, $institucion){

        $passphrase = $this->convertToHex(config('exa.sedes.'.$institucion.'.passphrase'));
        $ivtemp = config('exa.sedes.'.$institucion.'.iv');
        $integration = config('exa.sedes.'.$institucion.'.integration_name');
        $key_code = config('exa.sedes.'.$institucion.'.key_code');
        $urlpacs = config('exa.sedes.'.$institucion.'.pacs');
        $uname = auth()->user()->username_pacs;
        $formatDate = $this->getDate();

        $key = hex2bin($passphrase);
        $iv = hex2bin($ivtemp);
        $message = "integration_type=".$integration."&generated_dt=".$formatDate."&username=".$uname."&account_no=".$patientid."&flag=1&accession_no=".$ordernum;
        $padMsg = $this->padString($message);

        $encrypted = $this->encrypt($padMsg,$key,$iv);

        return $urlpacs.'/secure_link/code/'.$key_code.'/payload/'.$encrypted;
    }

    public function convertToHex($str){
        return bin2hex($str);
    }

    public function getDate(){
        $today = str_replace(" ", "+", Carbon::now()->toDateTimeString());
        return str_replace(":", "%3A", $today);
    }

    public function padString($source) {

        $paddingChar = ' ';
        $size = 16;
        $padLength = $size - strlen($source);

        for ($i = 0; $i < $padLength; $i++) {
            $source .= $paddingChar;
        }

        return $source;
    }

    public function encrypt($message,$key,$iv){

        $encrypted = openssl_encrypt($message, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);

        return bin2hex($encrypted);
    }

    public function obtenerIdAdministrativo($id){
        $sql = "SELECT administrative_id FROM institutions where id = '".$id."'";
        $institucion = DB::select($sql);

        return $institucion[0]->administrative_id;
    }

    public function getIdReferring(){
        // $sql = "SELECT id FROM `apimeditron`.`referrings` where user_id = '".auth()->user()->id."'";
        $sql = "SELECT id FROM `apimeditron`.`referrings` where id_user = '".auth()->user()->id."'";
        return DB::select($sql);
    }

    public function getRoleReferring($institution){
        $sql = "SELECT role_id FROM `mediris`.`charges` where user_id = '".auth()->user()->id."' AND institution_id = '".$institution."'";
        $role =  DB::select($sql);
        $role = json_encode($role);
        return json_decode($role, true)[0]['role_id'];
    }
}