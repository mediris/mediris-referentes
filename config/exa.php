<?php

return [

    'sedes' => [
        
        // DESARROLLO
        '99' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LMKWQBSI',
            'passphrase' => 'cQkkG97IOSJtE3MCsO2ZgOY6ns51smRv',
            'iv' => '6299c9de8f97b30a80731219a3ae6dcc',
            'url' => '',
            'pacs' => '',
            'id' => '1'
        ],

        // CIENCIAS
        '04' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LM6QNZZE',
            'passphrase' => 'dWrQAI9Ps5E9DUbRX7uLMNZs00DIvA1E',
            'iv' => 'e076e0e8bec9916ee0c0f5c9373ac476',
            'url' => 'http://medirisclc.idaca.com.ve',
            'pacs' => 'http://exaclc.viztek.net',
            'id' => '1'
        ], 
        
        // CMDLT
        '11' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LKR6CKSQ',
            'passphrase' => 'CxLXdlO5AyYQoUTIEklLFxCPRYNsKH28',
            'iv' => '320cb7a56172afb1b12f9de6a9380b62',
            'url' => 'http://mediriscdt.idaca.com.ve',
            'pacs' => 'http://exacdt.viztek.net',
            'id' => '1'
        ],
        
        // MNU
        '12' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LKR6CKSQ',
            'passphrase' => 'CxLXdlO5AyYQoUTIEklLFxCPRYNsKH28',
            'iv' => '320cb7a56172afb1b12f9de6a9380b62',
            'url' => 'http://medirismnu.idaca.com.ve',
            'pacs' => 'http://exacdt.viztek.net',
            'id' => '1'
        ],
        
        // OASIS
        '16' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LLY8FKL4',
            'passphrase' => 'MAoc1P1bXCsTuUktOyLknYQUhrqAxHxb',
            'iv' => '54f9845d1e8cdbafac12a9a29861edad',
            'url' => 'http://medirisoas.idaca.com.ve',
            'pacs' => 'http://exaoas.viztek.net',
            'id' => '1'
        ],
        
        // SANATRIX
        '19' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LMI3LYSG',
            'passphrase' => 'j0PbQayJCEIItOjJTMwnBXTPwKkrC2OU',
            'iv' => 'b637cfdb405017563e36fe79832a5663',
            'url' => 'http://medirissnx.idaca.com.ve',
            'pacs' => 'http://exasnx.viztek.net',
            'id' => '1'
        ],
        
        // FLORESTA
        '20' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LMAZ71XO',
            'passphrase' => 'TTuSyMcfAPpdOtvcdcHvhOK8OIr1Qd2h',
            'iv' => '02e5dd12db4a9749f27cde5060c0f25f',
            'url' => 'http://medirisflt.idaca.com.ve',
            'pacs' => 'http://exafrt.viztek.net',
            'id' => '1'
        ],
        
        // FENIX
        '21' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LM8341SY',
            'passphrase' => '6Q7N2wKJoQWCHiezXXvAbvSLnJKIk06t',
            'iv' => '508ba35f1e232d6d2899f3a875ee024a',
            'url' => 'http://medirisfnx.idaca.com.ve',
            'pacs' => 'http://exafnx.viztek.net',
            'id' => '1'
        ],
        
        // ARBOLEDA
        '22' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LM9HMG9T',
            'passphrase' => 'KLWclToOLWoDPIwnqmqYARYefSQrYvPj',
            'iv' => '8017c732ba36bbd9cf7ffc4d89b62637',
            'url' => 'http://medirisabl.idaca.com.ve',
            'pacs' => 'http://exaabl.viztek.net',
            'id' => '1'
        ],
        
        // HIGUEROTE
        '23' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LMJID01K',
            'passphrase' => '5gik13sjKVNji25LXk88cc9yI1nzIC5a',
            'iv' => '0f0405d35be3d320aa5ae53a4ad30e2b',
            'url' => 'http://medirishig.idaca.com.ve',
            'pacs' => 'http://exahig.viztek.net',
            'id' => '1'
        ],

        // LA VIÑA
        '25' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'M21VVMCH',
            'passphrase' => 'u9ah7Jth8QvRaLcOQa0wgEpUmefGYJEY',
            'iv' => '55744606d5d7a7fbb5c19b453ab36eb4',
            'url' => 'http://mediriscpv.idaca.com.ve',
            'pacs' => 'http://exacpv.viztek.net',
            'id' => '1'
        ],

        // NOVOMED
        '10' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LMKWQBSI',
            'passphrase' => 'cQkkG97IOSJtE3MCsO2ZgOY6ns51smRv',
            'iv' => '6299c9de8f97b30a80731219a3ae6dcc',
            'url' => 'http://medirisnvd.meditron.com.ve',
            'pacs' => 'http://exanvd.viztek.net',
            'id' => '1'
        ],

        // AVILA UNIDAD DE LA MUJER
        '01' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LMKWQBSI',
            'passphrase' => 'cQkkG97IOSJtE3MCsO2ZgOY6ns51smRv',
            'iv' => '6299c9de8f97b30a80731219a3ae6dcc',
            'url' => 'http://mediris.clinicaelavila.com',
            'pacs' => 'http://exanvd.viztek.net',
            'id' => '1'
        ],

        // AVILA UNIDAD DE IMAGEN
        '900' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LMKWQBSI',
            'passphrase' => 'cQkkG97IOSJtE3MCsO2ZgOY6ns51smRv',
            'iv' => '6299c9de8f97b30a80731219a3ae6dcc',
            'url' => 'http://mediris.clinicaelavila.com',
            'pacs' => 'http://exanvd.viztek.net',
            'id' => '3'
        ],

        // AVILA UNIDAD DE ULTRASONIDO
        '901' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LMKWQBSI',
            'passphrase' => 'cQkkG97IOSJtE3MCsO2ZgOY6ns51smRv',
            'iv' => '6299c9de8f97b30a80731219a3ae6dcc',
            'url' => 'http://mediris.clinicaelavila.com',
            'pacs' => 'http://exanvd.viztek.net',
            'id' => '6'
        ],

        // AVILA ECOCARDIOGRAFIA
        '902' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LMKWQBSI',
            'passphrase' => 'cQkkG97IOSJtE3MCsO2ZgOY6ns51smRv',
            'iv' => '6299c9de8f97b30a80731219a3ae6dcc',
            'url' => 'http://mediris.clinicaelavila.com',
            'pacs' => 'http://exanvd.viztek.net',
            'id' => '8'
        ],

        // LABORATORIO CLINICO EFICIENTE
        '02' => [
            'integration_name' => 'MEDITRON',
            'key_type' => 'AES-256',
            'key_code' => 'LPSFA9CG',
            'passphrase' => 'FdaND1XStZOHn64VrlhRRQsbWWu82ih8',
            'iv' => 'd4f8b1566beab96dd47cc35598a43dca',
            'url' => '',
            'pacs' => '',
            'id' => '1'
        ]
    ]
];

?>